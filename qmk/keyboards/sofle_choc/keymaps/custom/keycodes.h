#pragma once

enum {
  // Custom key for defining the tapdance allowing to transform C into Ç
  TD_C_CCED,
  // Transform the key W into SHIFT or CAPSLOCK
  TD_W_CAPSLOCK,
  // Transform the key E into È
  TD_EE,
  TD_PRC,
  TD_LAYER_SYMB,
  TD_LSFT,
};

enum {
  LAYER_BASE,
  LAYER_SYMBOLS,
  RIGHT_CTRL,
};

#define MENU    LGUI(BP_I)
#define KEY_APP LT(0, KC_APP)
#define KEY_C   TD(TD_C_CCED)
#define KEY_W   TD(TD_W_CAPSLOCK)
#define KEY_PRC TD(TD_PRC)
#define AL_ENT  MT(MOD_RALT, KC_ENT)
#define AL_SPC  MT(MOD_LALT, KC_SPC)
#define KEY_E   MT(MOD_LCTL, BP_E)
#define KEY_EE  TD(TD_EE)
#define KEY_T   MT(MOD_RCTL, BP_T)
#define KEY_INS MT(MOD_RGUI, KC_INS)
