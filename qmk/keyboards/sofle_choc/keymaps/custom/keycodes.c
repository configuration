#include QMK_KEYBOARD_H
#include "keymap_bepo.h"
#include "keycodes.h"

/*
 * Rules and modifier to apply over the keycodes. This includes the keys
 * redefinitions and the keys to include in the caps_word mecanism.
 *
 * All thoses update are working over the custom keys declared in `keycodes.h` 
 */

/*
 * This function need to be declared after the key definition, including the
 * tapdance keys because I need to reference the keycode here if I want to
 * include them in the caps_word mecanism.
 */
bool caps_word_press_user(uint16_t keycode) {
    switch (keycode) {
        // Keycodes that continue Caps Word, with shift applied.
        case KC_A ... KC_Z:
        case KC_1 ... KC_0:
        case KC_MINS:
        case KEY_C: // Add also the tapdance keys here.
        case KEY_W:
        case KEY_E:
        case BP_Z:  // Additionals keys from the bepo layout.
        case BP_M:
        case BP_G:
        case BP_H:
        case BP_N:
        case BP_F:
            add_weak_mods(MOD_BIT(KC_LSFT));  // Apply shift to next key.
            return true;

        // Keycodes that continue Caps Word, without shifting.
        case KC_BSPC:
        case KC_DEL:
            return true;
        case KC_SPACE:
            // The space key is used in order to generate the _ symbol,
            // I check which modifier is applied, it’s ok when it’s ALT
            return get_mods() & MOD_MASK_ALT;

        default:
            return false;  // Deactivate Caps Word.
    }
}

uint16_t get_tapping_term(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
        case KEY_E:
            return 230;
        case KEY_W:
            return 400;
        case KEY_EE:
            return 350;
        default:
            return TAPPING_TERM;
    }
}

static uint32_t key_timer;           // timer for last keyboard activity, use
                                     // 32bit value and function to make longer
                                     // idle time possible
static uint16_t latest_key = 0L;

bool process_record_user(uint16_t keycode, keyrecord_t *record) {


  if (!record->event.pressed) {
      // store time of last key release. This is used as a counter in order to
      // know if we are inside a typing sequence or not.
      key_timer = timer_read32();
      latest_key = keycode;
  }

  switch (keycode) {
    case KC_LEFT_SHIFT:
      if (host_keyboard_led_state().caps_lock) {
        tap_code16(KC_CAPS_LOCK);
      }
      return true;
    case AL_ENT:
      if (layer_state_is(LAYER_SYMBOLS) && !record->event.pressed) {
          // Remove the layer the key is released.
          layer_clear();
      }
      return true;
    // If a key where released just before, consider we are typing some text
    // and not starting a new sequence 
    case KEY_E:
      if (record->event.pressed && timer_elapsed32(key_timer) < TAPPING_TERM) {
          if (is_caps_word_on()) {
              add_oneshot_mods(MOD_MASK_SHIFT);
          }
          register_code(BP_E);
          return false;
      }
      return true;
    case KEY_T:
      if (record->event.pressed && timer_elapsed32(key_timer) < TAPPING_TERM) {
          if (is_caps_word_on()) {
              add_oneshot_mods(MOD_MASK_SHIFT);
          }
          register_code(BP_T);
          return false;
      }
      return true;
    // Here, the key KC_BSPC become È when used inside a sequence, but we still
    // allow the repetition of KC_BSPC.
    case KC_BSPC:
      if (record->event.pressed \
              && timer_elapsed32(key_timer) < (TAPPING_TERM / 2) \
              && latest_key != keycode)
      {
          // I completely rewrite the key here, that’s why I’m using tap_code16
          // instead of register_code.
          tap_code16(BP_EGRV);
          return false;
      }
      return true;

    // Override the key APP when hold into AltGR + Layer 1 when hold
    case KEY_APP:
      if (!record->tap.count && record->event.pressed) {
          layer_on(1);
          register_code(KC_RIGHT_ALT);
          return false;
      } else if (!record->tap.count && !record->event.pressed) {
          unregister_code(KC_RIGHT_ALT);
          layer_off(1);
          return false;
      }

    case KC_LGUI:
        if (record->event.pressed) {
          layer_on(1);
        }
        else {
          layer_off(1);
        }
        return true;
    default:
      return true; // Process all other keycodes normally
  }
}
