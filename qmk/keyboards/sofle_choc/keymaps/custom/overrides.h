#pragma once

#include "keymap_bepo.h"
#include "keycodes.h"

//
// Override the symbol ° and replace it by `
// The symbol is still available in the symbol layer with the key just below.
//
const key_override_t perc_key_override ;

// 
// I don’t care of the mapping CTRL+Ç and using the mod_tap does not work well
// when I type too fast because of tap-dance, so I remap the pattern here.
//
const key_override_t c_key_override ;

// Same here, I want to be able to type '' without triggering the tapdance.
// And there is no such key combo for Alt+È.
const key_override_t quote_key_override ;

// Same here, I override the key W with CTRL because the tapdance activate the 
// caps_word which does not make sense here.
const key_override_t w_key_override ;

const key_override_t e_key_override ;

// This globally defines all key overrides to be used
const key_override_t *key_overrides[]= (const key_override_t *[]){
    &perc_key_override,
    &c_key_override,
    &w_key_override,
    &e_key_override,
    &quote_key_override,
    // Override the key GUI + MENU and to map the key desktop from the
    // typematrix
    &(ko_make_basic(MOD_MASK_GUI, KEY_APP, MENU)),


    NULL
};
