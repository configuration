/* Copyright 2023 Brian Low
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "keymap_common.h"
#include "quantum_keycodes.h"
#include "process_tap_dance.h"
#include "process_key_override.h"

#include "keymap_bepo.h"
#include "keycodes.h"
#include "quad_tapdance.h"
#include "overrides.h"
#include QMK_KEYBOARD_H

// Declare the tapdance table here.
tap_dance_action_t tap_dance_actions[] = {
  [TD_C_CCED]           = ACTION_TAP_DANCE_DOUBLE(BP_C, BP_CCED),
  [TD_PRC]              = ACTION_TAP_DANCE_DOUBLE(BP_EQL, BP_PERC),
  [TD_W_CAPSLOCK]       = ACTION_TAP_DANCE_FN_ADVANCED(NULL, w_finished, w_reset),
  [TD_LAYER_SYMB]       = ACTION_TAP_DANCE_FN_ADVANCED(NULL, ql_finished, ql_reset),
  [TD_EE]               = ACTION_TAP_DANCE_DOUBLE(BP_COMM, BP_EGRV),
};



#define LY_SYMBOL TD(TD_LAYER_SYMB)


const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
/*
 * BÉPO
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * |  `   |   1  |   2  |   3  |   4  |   5  |                    |   6  |   7  |   8  |   9  |   0  |  `%  |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * | ESC  |   B  |   É  |   P  |   O  |   È  |                    |   !  |   V  |   D  |   L  |   J  |  Z   |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * | Tab  |   A  |   U  |   I  |   E  |  ;È  |-------.    ,-------|  CÇ  |   T  |   S  |   R  |   N  |  M   |
 * |------+------+------+------+------+------|  Mute |    | Pause |------+------+------+------+------+------|
 * |LShift|   À  |   Y  |   X  |   :  |   K  |-------|    |-------|   ?  |   Q  |   G  |   H  |   F  |  W   |
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *            | LCTL | LGUI | LCMD | Layer| / Space /       \Enter \  | Layer| RCMD | RGUI | RCTL |
 *            |      |      |      |  dwn |/ LALT  /         \ RALT \ |   up |      |      |      |
 *            `-----------------------------------'           '------''---------------------------'
 */

[LAYER_BASE] = LAYOUT(
    KC_ESC,   KC_1,   KC_2,    KC_3,    KC_4,    KC_5,                       KC_6,     KC_7,     KC_8,    KC_9,    KC_0,    KEY_PRC,
    BP_DLR,   BP_B,   BP_EACU, BP_P,    BP_O,    KC_BSPC,                    BP_DCIR,  BP_V,     BP_D,    BP_L,    BP_J,    BP_Z,
    KC_TAB,   BP_A,   BP_U,    BP_I,    KEY_E,   KEY_EE,                     KEY_C,    KEY_T,    BP_S,    BP_R,    BP_N,    BP_M,
    KC_LSFT,  BP_AGRV,BP_Y,    BP_X,    BP_DOT,  KC_B,    KC_MUTE,   KC_MPLY,BP_QUOT,  BP_Q,     BP_G,    BP_H,    BP_F,    KEY_W,
                      KC_LCTL, KC_DELETE,KC_LGUI,LY_SYMBOL,AL_SPC,   AL_ENT, LY_SYMBOL,KEY_APP,  KEY_INS, KC_RCTL
),

// In the number layout, I keep the mod-tap modifiers applies to the middle letters :
//
#define KEY_5      MT(MOD_LCTL, KC_P5)
#define KEY_DOWN   MT(MOD_RCTL, KC_DOWN)
#define KEY_PGDN   MT(MOD_RSFT, KC_PGDN)
#define EEPROM     QK_CLEAR_EEPROM

// This layer is used to access to the numeric keypad, functions keys, and also provide some keys used with KC_LGUI
[LAYER_SYMBOLS] = LAYOUT(
    KC_TRNS,  KC_F1,  KC_F2,   KC_F3,   KC_F4,   KC_F5,                      KC_F6,    KC_F7,    KC_F8,   KC_F9,   KC_F10,  KC_F11,
    EEPROM,   KC_NO,  KC_P7,   KC_P8,   KC_P9,   KC_TRNS,                    KC_HOME,  KC_UP,    KC_END,  KC_TRNS, KC_TRNS, S(BP_EQL),
    KC_TAB,   KC_NO,  KC_P4,   KEY_5,   KC_P6,   KC_NO,                      KC_LEFT,  KC_DOWN,  KC_RIGHT,KC_TRNS, KC_TRNS, KC_PGUP,
    KC_LSFT,  KC_NO,  KC_P1,   KC_P2,   KC_P3,   KC_NO,   KC_MUTE,   KC_MPLY,KC_TRNS,  KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KEY_PGDN,
                      KC_P0,   KC_PDOT, KC_TRNS, LY_SYMBOL,AL_SPC,   AL_ENT, LY_SYMBOL,KC_TRNS,  KC_RGUI, KC_TRNS
),
};

#if defined(ENCODER_MAP_ENABLE)
const uint16_t PROGMEM encoder_map[][NUM_ENCODERS][NUM_DIRECTIONS] = {
    [LAYER_BASE] = { ENCODER_CCW_CW(KC_VOLD, KC_VOLU), ENCODER_CCW_CW(KC_MS_WH_UP, KC_MS_WH_DOWN) },
    [LAYER_SYMBOLS] = { ENCODER_CCW_CW(KC_TRNS, KC_TRNS), ENCODER_CCW_CW(KC_TRNS, KC_TRNS) },
};
#endif
