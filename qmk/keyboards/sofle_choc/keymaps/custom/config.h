/* Copyright 2023 Brian Low
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

// Enabling this option changes the startup behavior to listen for an
// active USB communication to delegate which part is master and which
// is slave. With this option enabled and theres’s USB communication,
// then that half assumes it is the master, otherwise it assumes it
// is the slave.
//
// I've found this helps with some ProMicros where the slave does not boot
#define SPLIT_USB_DETECT

#define RGB_DISABLE_WHEN_USB_SUSPENDED     // turn off effects when suspended
#define SPLIT_TRANSPORT_MIRROR             // If LED_MATRIX_KEYPRESSES or LED_MATRIX_KEYRELEASES is enabled, you also will want to enable SPLIT_TRANSPORT_MIRROR
#define RGB_MATRIX_MAXIMUM_BRIGHTNESS 150  // limits maximum brightness of LEDs (max 255). Higher may cause the controller to crash.

// Sets the default enabled state, if none has been set
#define RGB_MATRIX_DEFAULT_ON true

// Triggers RGB keypress events on key down. This makes RGB control feel more
// responsive. This may cause RGB to not function properly on some boards
#define RGB_TRIGGER_ON_KEYDOWN      

// Disable the codes, don’t need them.
#define RGB_MATRIX_DISABLE_KEYCODES

#define ENABLE_RGB_MATRIX_BREATHING
#define ENABLE_RGB_MATRIX_SOLID_COLOR
#define ENABLE_RGB_MATRIX_SOLID_REACTIVE_SIMPLE
 // Sets the default mode, if none has been set
#define RGB_MATRIX_DEFAULT_MODE RGB_MATRIX_SOLID_REACTIVE_SIMPLE

// Sync data between the two splits. This allow the leds to be in sync in the
// two sides.
#define SPLIT_LAYER_STATE_ENABLE
#define SPLIT_LED_STATE_ENABLE
#define SPLIT_MODS_ENABLE

#define RP2040_BOOTLOADER_DOUBLE_TAP_RESET // Activates the double-tap behavior
#define RP2040_BOOTLOADER_DOUBLE_TAP_RESET_TIMEOUT 200U // Timeout window in ms in which the double tap can occur.
#define RP2040_BOOTLOADER_DOUBLE_TAP_RESET_LED D5 // Specify an optional status LED by GPIO number which blinks when entering the bootloader

#define TAPPING_TERM 200
#define TAPPING_TERM_PER_KEY

// Modify the tap-hold configuration for helping with some tap-dance keys.
// The behavior with double-tap (like c/ç) and mod-tap is to wait the delay for
// the tap-dance before reporting the key to send.
// If I release the MOD-TAP key before, the modifier is not applied and the
// host receive a sequence of two keys, which is not what I want.
//#define PERMISSIVE_HOLD
//#define HOLD_ON_OTHER_KEY_PRESS

#define COMBO_MUST_PRESS_IN_ORDER
