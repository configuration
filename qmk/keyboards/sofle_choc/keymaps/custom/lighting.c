#include QMK_KEYBOARD_H
    uint8_t flags = 0;
#include "keycodes.h"

// Set the color at the given position, but limit the intensity
void set_color(uint8_t index, uint8_t h, uint8_t s, uint8_t _) {
  HSV hsv = {h, s, RGB_MATRIX_MAXIMUM_BRIGHTNESS};
  RGB rgb = hsv_to_rgb(hsv);
  rgb_matrix_set_color(index, rgb.r, rgb.g, rgb.b);
}

HSV hsv_of_color(uint8_t h, uint8_t s, uint8_t v) {
    return (HSV){h, s, v > 100 ? 100:v};
}

#define KEY_5      MT(MOD_LCTL, KC_P5)
#define KEY_DOWN   MT(MOD_RCTL, KC_DOWN)
bool rgb_matrix_indicators_advanced_user(uint8_t led_min, uint8_t led_max) {
    // Activate effects depending of mods
 
    // Colors for the mods
    HSV const colors[] = {
        hsv_of_color(HSV_BLACK), // 000
        hsv_of_color(55, 255, 255),// 001
        hsv_of_color(HSV_RED),   // 010
        hsv_of_color(HSV_ORANGE),// 011
        hsv_of_color(HSV_CYAN),  // 100
        hsv_of_color(HSV_GREEN), // 101
        hsv_of_color(HSV_PURPLE),// 110
        hsv_of_color(HSV_WHITE), // 111
    };

    if (is_caps_word_on()) {
        if (rgb_matrix_get_mode() != RGB_MATRIX_BREATHING) {
            rgb_matrix_mode_noeeprom(RGB_MATRIX_BREATHING);
        }
        rgb_matrix_sethsv_noeeprom(HSV_SPRINGGREEN);
        return false;
    }

    uint8_t counter = 0;
    uint8_t flags = 0;
    if (get_mods() & MOD_MASK_CTRL) {
        flags |= 0b001;
        counter += 1;
    }
    if (get_mods() & MOD_MASK_ALT) {
        flags |= 0b010;
        counter += 1;
    }
    if (get_mods() & MOD_MASK_SHIFT) {
        flags |= 0b100;
        counter += 1;
    }

    if (get_highest_layer(layer_state) > 0) {
        for (uint8_t row = 0; row < MATRIX_ROWS; ++row) {
            for (uint8_t col = 0; col < MATRIX_COLS; ++col) {
                uint8_t index = g_led_config.matrix_co[row][col];

                if (index < led_min || index > led_max || index == NO_LED)
                    continue;

                uint8_t layer = layer_switch_get_layer((keypos_t){col,row});
                if (layer == 0)
                    continue;

                uint16_t keycode = keymap_key_to_keycode(layer, (keypos_t){col,row});

                switch (keycode) {
                    case KC_F1 ... KC_F11:
                        rgb_matrix_set_color(index, 128, 128, 128);
                        break;
                    case KC_P1 ... KC_P0:
                    case KEY_5:
                        rgb_matrix_set_color(index, 100, 100, 0);
                        break;
                    case KC_RIGHT:
                    case KEY_DOWN:
                    case KC_LEFT ... KC_UP:
                    case KC_PGUP:
                    case KC_PGDN:
                        rgb_matrix_set_color(index, 32, 32, 128);
                        break;
                    case KC_HOME:
                    case KC_END:
                        rgb_matrix_set_color(index, RGB_SPRINGGREEN);
                        break;
                    case AL_SPC:
                    case AL_ENT:
                        set_color(index, HSV_BLUE);
                        break;
                }

            }
        }
    }

    if (get_mods()) {
        if (rgb_matrix_get_mode() != RGB_MATRIX_SOLID_COLOR) {
            rgb_matrix_mode_noeeprom(RGB_MATRIX_SOLID_COLOR);
        }
        uint8_t v = colors[flags].v;
        if (counter == 2)
            v *= 2;
        rgb_matrix_sethsv_noeeprom(colors[flags].h, colors[flags].s, v);
    } else if (rgb_matrix_get_mode() != RGB_MATRIX_SOLID_REACTIVE_SIMPLE) {
        rgb_matrix_sethsv_noeeprom(HSV_WHITE);
        rgb_matrix_mode_noeeprom(RGB_MATRIX_SOLID_REACTIVE_SIMPLE);
    }

    return false;
}
