#!/bin/sh
BIN_PATH=/home/sebastien/Projets/python/serial/
LAYER_PATH=${BIN_PATH}/layers/

set -u
set -e

if [ -z ${1+x} ]; then
  ls ${LAYER_PATH}
else
  python3 ${BIN_PATH}/client.py ${BIN_PATH}/config_client.ini --layer ${LAYER_PATH}/$1 > /dev/null
fi
