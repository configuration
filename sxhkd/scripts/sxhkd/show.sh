#!/bin/sh

# Check if a desktop match the name
desktop=$(bspc query --desktops --names | grep "$1")

# If the required desktop does not exists, create it
if [ -z "$desktop" ]; then 
    bspc monitor --add-desktops "$1"
fi

# Switch to the desired desktop
bspc desktop --focus "${desktop:-$1}"
