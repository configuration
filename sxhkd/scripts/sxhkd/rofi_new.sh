#!/bin/sh

set -u
set -e

tmpdir=$(mktemp -d)

on_exit() {
    rm -r "${tmpdir}"
}

on_preExit() {
    on_exit
}

trap on_exit EXIT                           # EXIT = 0
trap on_preExit HUP INT QUIT TERM STOP PWR  # 1 2 3 15 30

if [ -z ${1+x} ]; then
    # We are using dash, the process substitution syntax is not allowed
    # sort -u <(…) <(…)
    # so, we are using pipes for that
    mkfifo ${tmpdir}/p1 ${tmpdir}/p2
    echo "Chat\nWeb\nMail\nmusique\nDiscord" > "${tmpdir}/p1" &
    bspc query --names -D > "${tmpdir}/p2" &
    sort -u "${tmpdir}/p1" "${tmpdir}/p2"
else
  ~/scripts/sxhkd/show.sh $1
fi

