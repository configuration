#!/bin/sh
if [ -z $1 ]; then
    echo "Flip"
    echo "Horaire"
    echo "Antihoraire"
  exit 0
else
  case $1 in
      Horaire)
        bspc node @parent --rotate 90
          ;;
      Antihoraire)
        bspc node @parent --rotate 270
          ;;
      Flip)
        bspc node @parent --rotate 180
          ;;
  esac
  exit 1
fi

