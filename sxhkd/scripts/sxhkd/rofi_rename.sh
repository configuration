#!/bin/sh

/bin/echo -en "\0prompt\x1fRenomer\n"

if [ -z $1 ]; then
  bspc query --desktops --names
  exit 0
else
  desktop=$(bspc query --desktops --names | grep "$1")
  
  # If the required desktop does not exists, rename it
  if [ -z "$desktop" ]; then 
      bspc desktop -n ${1}
  else 
      bspc desktop --focus "${desktop:-$1}"
  fi
  exit 1
fi
