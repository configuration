.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

.. default-role:: literal

Description
===========

Ce repo contient l'ensemble des fichiers de configuration pour les différentes
applications.

Clone et installation
=====================

.. code-block:: bash

  git clone http://git.chimrod.com/configuration.git/

Utilisation
===========

Se placer dans le répertoire courant, et lancer la commande pour chaque
répertoire `stow i3` pour installer les fichiers présents dans le répertoire.

Voir `man stow` pour plus de détail sur l'Utilisation.

Destination des répertoires
===========================

La plupart des dossiers ici sont destinés à etre installés dans le répertoire
`HOME`. Toutefois, certains sont à installer dans un sous-répertoire d’un
projet particulier pour fonctionner correctement.

:qmk:

    Le dossier est à installer dans le répertoire de qmk, avec l’option
    suivante :

    .. code:: bash

        stow --target ~/qmk_firmware/ qmk/

:keyboards:

    Le dossiers est à installer dans /
