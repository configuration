#!/bin/sh

. "${HOME}/.cache/wal/colors.sh"
polybar_conf=~/.config/polybar/colors.ini
echo "color0=$color0" >  "${polybar_conf}"
echo "color1=$color1" >> "${polybar_conf}"
echo "color2=$color2" >> "${polybar_conf}"
echo "color3=$color3" >> "${polybar_conf}"
echo "color4=$color4" >> "${polybar_conf}"
echo "color5=$color5" >> "${polybar_conf}"
echo "color6=$color6" >> "${polybar_conf}"
echo "color7=$color7" >> "${polybar_conf}"
echo "color8=$color8" >> "${polybar_conf}"
echo "color9=$color9" >> "${polybar_conf}"
echo "color10=$color10" >> "${polybar_conf}"
echo "color11=$color11" >> "${polybar_conf}"
echo "color12=$color12" >> "${polybar_conf}"
echo "color13=$color13" >> "${polybar_conf}"
echo "color14=$color14" >> "${polybar_conf}"
echo "color15=$color15" >> "${polybar_conf}"


xressource_conf=~/.config/xressource_colors
echo "#define _color0	$color0" >   "${xressource_conf}"
echo "#define _color1	$color1" >>  "${xressource_conf}"
echo "#define _color2	$color2" >>  "${xressource_conf}"
echo "#define _color3	$color3" >>  "${xressource_conf}"
echo "#define _color4	$color4" >>  "${xressource_conf}"
echo "#define _color5	$color5" >>  "${xressource_conf}"
echo "#define _color6	$color6" >>  "${xressource_conf}"
echo "#define _color7	$color7" >>  "${xressource_conf}"
echo "#define _color8	$color8" >>  "${xressource_conf}"
echo "#define _color9	$color9" >>  "${xressource_conf}"
echo "#define _color10	$color10" >> "${xressource_conf}"
echo "#define _color11	$color11" >> "${xressource_conf}"
echo "#define _color12	$color12" >> "${xressource_conf}"
echo "#define _color13	$color13" >> "${xressource_conf}"
echo "#define _color14	$color14" >> "${xressource_conf}"
echo "#define _color15	$color15" >> "${xressource_conf}"


