t=0

draw() {
    if [ $t -eq 0 ]; then
        date +%H:%M
    else
        date +"%d %b %H:%M"
    fi
}

toggle() {
    t=$(((t + 1) % 2));
    draw
}


trap "toggle" USR1

while true; do
    draw
    sleep 60 &
    wait
done
