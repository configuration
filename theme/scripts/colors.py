#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import colorsys

def define(saturation, value):

    def showColor(hue):
        return colorsys.hsv_to_rgb((1.0 + hue)%1.0, (1.0 + saturation)%1.0, (1.0 + value)%1.0)

    return showColor


class Hering(object):
    """ Create the palette with the Hering circle
    https://fr.wikipedia.org/wiki/Cercle_chromatique#Hering
    """

    def of_value(self, angle):
        angle = (angle + 360) % 360
        if angle < 120:
            return (angle * 1.5)
        else:
            return (180 + (angle - 120) * 0.75)

    def to_value(self, angle):
        angle = (angle + 360) % 360
        if angle < 180:
            return (angle * 2. / 3.)
        else:
            return (120 + ((angle - 180) * 4. / 3.))

def showColor(theme, hue):

    r, g, b = theme(hue / 360.)
    return "#%02x%02x%02x" % (int(r*256), int(g*256), int(b*256))

if __name__ == "__main__":

    if len(sys.argv) < 5:
        print "Usage :"
        print "color.py LIGHT_SATURATION DARK_SATURATION LIGHT_VALUE DARK_VALUE MIN_ANGLE MAX_ANGLE"
        sys.exit(1)

    shift = 0
    light_s = int(sys.argv[1])/100.
    dark_s =  int(sys.argv[2])/100.
    light_v = int(sys.argv[3])/100.
    dark_v =  int(sys.argv[4])/100.
    angle =   int(sys.argv[5])
    angle2 =  int(sys.argv[6])

    colors = [

        0,    # red
        2,   # yellow
        1,   # green
        5,  # cyan
        3,  # blue
        4,  # magenta

    ]

    h = Hering()

    print "!This theme has been generated with the command"
    print "!colors.py %s %s %s %s %s %s" % (sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6])
    dark_theme = define(dark_s, dark_v)
    light_theme = define(light_s, light_v)
    h_angle = h.of_value(angle)
    h_angle2 = h.of_value(angle2)
    delta = (360 + (h_angle2 - h_angle))% 360 / 6.0
    for name in colors:

        value = h.to_value(angle + name * delta)

        dark  = showColor(dark_theme, value)
        light = showColor(light_theme, value)

        print "#define _color%s %s" % (name+1, dark)
        print "#define _color%s %s" % (name+9, light)
        #print "urxvt.color%d : %s" % (name, dark)
        #print "urxvt.color%d : %s" % (name+8, light)

        print "XTerm*color%d : %s" % (name+1, dark)
        print "XTerm*color%d : %s" % (name+9, light)


