#!/bin/sh

if [ -z "$1" ]; then
  echo ""
else
  if [ -e "$1" ]; then
    xdg-open "$1" > /dev/null
  else
    echo ""
    locate --ignore-case -A ~ $* | grep -v "/\."
  fi
fi

