#!/bin/sh
DB_FILE=~/.mozilla/firefox/qoh6qw4f.default/places.sqlite

if [ -z $1 ]; then
  sqlite3 "${DB_FILE}" 'SELECT url from moz_places order by frecency desc' | grep -vE "q=|place:|moz-extension"
  exit 0
else
  i3-msg "exec firefox --new-tab \"$*\"" > /dev/null
  exit 1
fi
