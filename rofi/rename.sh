#!/bin/sh
if [ -z $1 ]; then
    echo "Edit:"
    echo "Musique:"
  exit 0
else
  i3-msg "rename workspace to ${1}" > /dev/null
  exit 1
fi
