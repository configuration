#!/bin/sh
WORKSPACES=~/.config/i3/workspaces

if [ -z $1 ]; then
  for fic in ~/.config/i3/workspaces/*.json; do 
      echo $(basename "$fic" .json)
  done
  exit 0
else
  WINDOWS=$(xdotool search --all --onlyvisible --desktop $(xprop -notype -root _NET_CURRENT_DESKTOP | cut -c 24-) "" 2>/dev/null)
  code=$(echo $1 | cut -d: -f1)
  # Load the workspace
  i3-msg "workspace ${code}:; append_layout ${WORKSPACES}/${1}.json; workspace back_and_forth" > /dev/null
  # get the list of windows on the current workspace
  for window in $WINDOWS; do
    HAS_PID=$(xdotool getwindowpid $window 2>&1 | grep "pid" | wc -l)
  
    # Unmap the window to make it disapear, then map it again for i3 trigger the swallow
    xdotool windowunmap $window
    xdotool windowmap $window
  done
  i3-msg "workspace ${code}:" > /dev/null
  exit 1
fi
