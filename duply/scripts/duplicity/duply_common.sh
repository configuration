JSON_CONF=$(mktemp)
cleanup() {
    rm -f "${JSON_CONF}"
}
trap cleanup INT TERM EXIT

gpg_export_if_needed() {
    echo "Prevent GPG keys for beeing exported"
}

# Always mark the secret key as available (do not try to import it)
gpg_sec_avail() {
    true
}

# Override the read command with zenity
read() {
  if command -v zenity &> /dev/null
  then
    RESULT=$(zenity --password 2>/dev/null) 
    if [ $? -ne 0 ]; then
      command read $*
  else
      eval $2=${RESULT}
    fi
  else
    command read $*
  fi
}
. ~/.config/ovh/duplicity-$(whoami).sh

#Generate the configuration
case $2 in 
    restore)
        export COLD_PATH=$(zenity --file-selection --directory --title="Chemin des sauvegardes")
        ~/scripts/duplicity/gen_config_restore.sh "${JSON_CONF}"
        ;;
    *)
        ~/scripts/duplicity/gen_config_remote.sh "${JSON_CONF}"
        ;;
esac

TARGET="multi:${JSON_CONF}?mode=mirror&onfail=abort"

DUPL_PARAMS="$DUPL_PARAMS --file-prefix-manifest 'hot_' --file-prefix-signature 'hot_' --file-prefix-archive 'cold_' "
