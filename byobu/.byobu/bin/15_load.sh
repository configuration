#!/bin/sh -e

BLACK="\033[00m"
YELLOW="\033[1;33m"

load=`cat /proc/loadavg | sed -e 's/ .*//'`
[ -n "$load" ] || return

if [ $(echo "$load < 0.5" | bc) = 1 ]
then
    return
    printf "<0.5"
else

    printf "#[bg=#FFFF00]$load"
    echo 
fi
