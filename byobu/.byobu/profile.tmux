source $BYOBU_PREFIX/share/byobu/profiles/tmux

# Define the terminal to be 256 colors (override default value)
set -g default-terminal "screen-256color"


set-option -g set-titles on
set-option -g set-titles-string "#W(#h)"

