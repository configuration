#!/bin/sh

# Traverse the chromatic circle in 5 hours
cutoff=18000
background="#eceff4"

# L’angle tourne toute les secondes
angle=$(echo "$(date +%s) % 360" | bc)
dominant=$(python3 -c "import colorsys ; import datetime ; now = datetime.datetime.now() ; min = datetime.datetime.min ; sec = (now - min).seconds % $cutoff ; print(''.join('%02x' % int(i*255) for i in colorsys.hsv_to_rgb(sec/$cutoff, 0.35, 1)))")
background=$(python3 -c "import colorsys ; import datetime ; now = datetime.datetime.now() ; min = datetime.datetime.min ; sec = (now - min).seconds % $cutoff ; print(''.join('%02x' % int(i*255) for i in colorsys.hsv_to_rgb(sec/2000, 0.2, 1)))")
#background='ff0000'

hsetroot \
-add "#${dominant}"  \
-add  "#${background}" \
-gradient ${angle}
