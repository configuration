#!/bin/sh

set -u
#set -e

# Execute an application when creating the desktop
# 
run_desktop() {
    name=$1
    application=$2
    window_name=$3
    bspc node @$name:/ -i
    bspc rule --add $window_name --one-shot node=@$name:/ follow=off focus=off
    $application &
}


# This script is designed to be used with sxhkd/rofi_new.sh which populate the
# rof menu with the values.
bspc subscribe desktop | while read -r event monitor_id desktop_id name; do
    case $event in 
        desktop_focus)
            ~/scripts/bspwm/wallpaper.sh &
            for desktop in $(bspc query -D); do
                test "x${desktop}" = "x${desktop_id}" && continue

                # If the desktop is empty, delete it
                nodes=$(bspc query -d ${desktop} -N)
                test -z "${nodes}" && bspc desktop ${desktop} -r 
            done
            ;;
        desktop_add)
            bspc monitor -o $(bspc query --names -D | sort -V | tr -s '\n' ' ')
            case $name in 
                Chat)
                    run_desktop $name signal-desktop Signal
                    ;;
                Musique)
                    run_desktop $name gmpc Gmpc
                    ;;
                Web)
                    run_desktop $name firefox-esr firefox-esr
                    ;;
                Mail)
                    run_desktop $name thunderbird thunderbird-default
                    ;;
                Discord)
                    run_desktop $name discord discord
                    ;;
            esac
            ;;
        desktop_rename)
            bspc monitor -o $(bspc query --names -D | sort -V | tr -s '\n' ' ')
            ;;
    esac
done
