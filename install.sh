#!/bin/sh

echo "Installation du layout bépo pour le clavier typematrix"
install -m 644 keyboards/etc/X11/xorg.conf.d/typematrix.conf /etc/X11/xorg.conf.d
install -m 644 keyboards/etc/X11/xorg.conf.d/sofle.conf /etc/X11/xorg.conf.d

echo "Driver udev pour flipperzero"
install -m 644 flipperzero/etc/udev/rules.d/42-flipperzero.rules /etc/udev/rules.d 
